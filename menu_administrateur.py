from serie import Serie
from base_serie import BaseSeries
import pandas as pd

# Fonction pour afficher le menu administrateur


def menu_administrateur(base_series):
    while True:
        print("Bienvenue dans le menu administrateur :")
        print("0. Charger une base de série")
        print("1. Chercher une série")
        print("2. Modifier une série existante")
        print("3. Ajouter une nouvelle série")
        print("4. Supprimer une série")
        print("5. Quitter")

        choix = input("Entrez votre choix : ")

        if choix == '0':
            file = input("Entrez le nom de votre base de données: ")
            if not file.endswith('.csv'):
                raise TypeError("La base de donnée doit être au format csv")
            else:
                base_series = BaseSeries()
                base_series.load_from_path(file)
        elif choix == '1':
            titre = input("Entrez le titre de la série à rechercher : ")
            try:
                serie = base_series.search_serie(titre)
                print(f"Série trouvée : {serie}")
            except KeyError as e:
                print(e)
        elif choix == '2':
            try:
                serie_id = int(input("Entrez l'ID de la série à modifier : "))
                if serie_id == 456:
                    raise OSError
                serie = base_series.series.get(456)
                attribut = input("Entrez le nom de l'attribut à modifier : ")
                getattr(serie, attribut)  # Tentative de récupération de
                #                            l'attribut
                nouvelle_valeur = input(
                    f"Entrez la nouvelle valeur pour l'attribut '{attribut}': "
                )
            except OSError:
                print(
                    "Pour le bon fonctionnement de cette application, cet id \
                        ne peut etre modifier"
                )
            except ValueError:
                print(
                    "Vous devez entrer un entier pour l'ID de la série !!!"
                )
            except AttributeError:
                print("Cet attribut n'existe pas dans la classe Serie.")
            else:
                try:

                    # Vérifier si l'attribut est de type entier
                    if attribut in ['id_serie', 'number_of_seasons',
                                    'number_of_episodes']:
                        nouvelle_valeur = int(nouvelle_valeur)
                    # Vérifier si l'attribut est de type booléen
                    elif attribut == 'adult':
                        if nouvelle_valeur.lower() == 'true':
                            nouvelle_valeur = True
                        elif nouvelle_valeur.lower() == 'false':
                            nouvelle_valeur = False
                        else:
                            raise ValueError(
                                "La nouvelle valeur pour 'adult' doit être\
                                    'true' ou 'false'."
                            )
                    # Vérifier si l'attribut est de type datetime
                    elif attribut in ['first_air_date', 'last_air_date']:
                        nouvelle_valeur = pd.to_datetime(nouvelle_valeur)
                except ValueError:
                    print("Il y a une incoherence entre la nouvelle valeur et\
                        l'attribut à modifier")
                except Exception as e:
                    print(f"Une erreur est survenue : {e}")
                else:
                    serie = base_series.series.get(serie_id)
                    if serie:
                        setattr(serie, attribut, nouvelle_valeur)
                        # Mettre à jour la série dans la base
                        base_series.series[serie_id] = serie
                        print("La série a bien été modifiée")
                        # Sauvegarder la base de données dans un fichier après
                        # chaque ajout de série
                        # base_series.save_to_file(file_name)
                    else:
                        print("Aucune série trouvée avec cet ID")
        elif choix == '3':
            print("Ajout d'une nouvelle série : ")
            # Saisie des détails de la nouvelle série...
            name = input("Entrez le nom de la nouvelle série : ")
            spoken_languages = input(
                "Entrez les langues (séparées par des virgules) : ").split(',')

            status = input("Entrez le statut de la série : ")
            genres = input(
                "Entrez les genres (séparés par des virgules) : ").split(',')
            networks = input(
                "Entrez les réseaux de distribution"
                "(séparés par des virgules) : \
                    ").split(',')
            try:
                id_serie = int(
                    input("Entrez l'ID de la nouvelle série : ")
                )
                number_of_seasons = int(
                    input("Entrez le nombre de saisons : ")
                )
                number_of_episodes = int(
                    input("Entrez le nombre d'épisodes : ")
                )
                first_air_date = pd.to_datetime(input(
                    "Entrez la date de première diffusion (AAAA-MM-JJ) : "
                ), format="%Y-%m-%d")
                last_air_date = pd.to_datetime(input(
                    "Entrez la date de dernière diffusion (AAAA-MM-JJ) : "
                ), format='%Y-%m-%d')
                adult = input(
                    "La série est-elle destinée aux adultes ? (True/False) : \
                    ").lower()
                if adult == 'true':
                    adult = True
                elif adult == 'false':
                    adult = False
                else:
                    raise ValueError("La réponse doit être 'True' ou 'False'.")
            except ValueError:
                print(
                    "Erreur, Valeur incorrecte"
                )
            except Exception as e:
                print(f"Erreur : {e}")
            else:

                # Créer un objet Serie avec les détails saisis
                new_serie = Serie(
                    id_serie=id_serie,
                    name=name,
                    number_of_seasons=number_of_seasons,
                    number_of_episodes=number_of_episodes,
                    spoken_languages=spoken_languages,
                    first_air_date=first_air_date,
                    last_air_date=last_air_date,
                    status=status,
                    genres=genres,
                    networks=networks,
                    adult=adult
                )

                base_series.add_serie(new_serie)

        elif choix == '4':
            serie_id = int(input("Entrez l'ID de la série à supprimer : "))
            base_series.delete_serie(serie_id)
        elif choix == '5':
            print("Merci d'avoir utilisé le menu administrateur. Au revoir !")
            break
        else:
            print("Choix invalide. Veuillez sélectionner une option valide.")


"""if __name__ == "__main__":
    base_series = BaseSeries()
    # Charger la base de séries à partir du fichier CSV
    base_series.load_from_path("dat_test.csv")
    menu_administrateur(base_series)"""

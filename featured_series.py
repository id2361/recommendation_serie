import datetime
from base_series import BaseSeries


class FeaturedSeries:
    """Classe représentant une série inédite.

    Attributes:
    -----------
    base_series : BaseSeries
        Base de données contenant les séries.

    Parameters:
    -----------
    base_series : BaseSeries
        Base de données contenant les séries.

    Examples:
    ---------
    >>> base_series1 = BaseSeries()
    >>> base_series1.load_from_path("serie_test.csv")
    Base de données chargée avec succès depuis serie_test.csv.
    >>> get_new_serie(base_series1)
    Séries récentes (sorties il y a moins d'un an) :
    - Riverdale
    - The Flash
    - Grey's Anatomy
    - The Simpsons
    """
    def __init__(self, base_series):
        if not isinstance(base_series, BaseSeries):
            raise TypeError("base_series doit être une instance de "
                            "BaseSeries.")
        self.base_series = base_series

    def featured_series_less_year(self):
        """Obtention des séries sorties il y a moins d'un an.

        Return:
        -------
        list
            Liste des séries sorties il y a moins d'un an.
        """
        released_series = []
        date_actuelle = datetime.datetime.now()

        for serie in self.base_series.series.values():
            if (date_actuelle - serie.last_air_date).days <= 365:
                released_series.append(serie)

        return released_series

def get_new_serie(base_series):
    """Procédure qui affiche les dernières séries sorties (moins d'un an).

    Parameters:
    -----------
    base_series:
        Base de données contenant les séries.
    """
    a_la_une = FeaturedSeries(base_series)
    series_recentes = a_la_une.featured_series_less_year()
    print("Séries récentes (sorties il y a moins d'un an) :")
    for serie in series_recentes:
        print(f"- {serie.name}")


if __name__ == "__main__":
    import doctest
    doctest.testmod(verbose=True)

from serie import Serie
from base_series import BaseSeries


class Genre:
    """Classe représentant un genre de série.

    Attributes:
    -----------
    name : str
        Le nom du genre.

    Examples:
    ---------
    >>> base_series = BaseSeries()
    >>> base_series.load_from_path("test_serie.csv")
    Base de données chargée avec succès depuis test_serie.csv.
    >>> drama=Genre("Drama")
    >>> list_drama=drama.series_of_genre(base_series)
    >>> print(list_drama[0])
    Game of Thrones
    >>> serie=base_series.series[66732]
    >>> drama.verify_serie_genre(serie)
    True
    """
    def __init__(self, name: str):
        if not isinstance(name, str):
            raise TypeError("Le nom doit être une instance de str.")
        self.name = name

    def series_of_genre(self, base_series):
        """Récupèraion de toutes les séries appartenant
        à ce genre dans la base de données spécifiée.

        Parameters:
        -----------
        base_series: BaseSeries
            Base de données contenant les séries.

        Return:
        -------
        list[str]
            Liste des séries appartenant au genre.
        """
        if not isinstance(base_series, BaseSeries):
            raise TypeError("La base de données doit être une instance de "
                            "BaseSeries.")
        series_genre = []
        for serie in base_series.series.values():
            if self.name in serie.genres:
                series_genre.append(serie)
        return series_genre

    def verify_serie_genre(self, serie):
        """Vérification de l'appartenance d'une série appartient au genre.

        Parameters:
        -----------
        serie: Serie
            Série dont nous souhaitons vérifier son appartenance au genre.

        Return:
        -------
        bool
            True si la série appartient bien au genre, False sinon.
        """
        if not isinstance(serie, Serie):
            raise TypeError("La série doit être une instance de Serie.")
        return self.name in serie.genres


def get_series_by_genre(genre_name, base_series):
    """Procédure affichant les séries appartenant à un genre.

    Parameters:
    -----------
    genre_name: str
        Nom du genre.
    base_series: BaseSeries
        Base de données contenant les séries.
    """
    genre = Genre(genre_name)
    series = genre.series_of_genre(base_series)
    print(f"Séries du genre '{genre_name}':")
    for serie in series:
        print(serie.name)


if __name__ == "__main__":
    import doctest
    doctest.testmod(verbose=True)

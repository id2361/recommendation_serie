import csv
from user import User


class BaseUser:
    """Classe représentant une base de données d'utilisateurs.

    Attributes:
    -----------
    file_path : str
        Chemin vers le fichier CSV contenant les informations des utilisateurs.

    Parameters:
    -----------
    file_path : str, optional
        Chemin vers le fichier CSV contenant les informations des utilisateurs.
        Par défaut, "data_users.csv".

    Examples:
    ---------
    Création d'une instance de BaseUser avec un fichier CSV existant :

    >>> base_user = BaseUser("data_users.csv")

    >>> base_user.add_user("Alice", "mdp123", True, "fr", "Drame")
    Utilisateur ajouté avec succès.


    Recherche et chargement d'un utilisateur existant :

    >>> user = base_user.load_user("Alice")
    >>> user.user_name
    'Alice'
    >>> user.adult
    True


    Modification des informations d'un utilisateur :

    >>> base_user.modify_user(
    ...     "Alice", new_language="en", new_genre_serie="Comédie"
    ... )
    Les informations utilisateur ont été mises à jour.
    True

    Vérification de l'existence d'un utilisateur dans la base :

    >>> base_user.verify_user("Alice")
    True


    Suppression d'un utilisateur de la base :

    >>> base_user.delete_user("Alice")
    Utilisateur supprimé avec succès.


    Vérification que l'utilisateur a été supprimé :

    >>> base_user.verify_user("Alice")
    False
    """
    def __init__(self, file_path="data_users.csv"):
        self.file_path = file_path

    def load_user(self, user_name):
        """Chargerment des informations d'un utilisateur à partir du
        fichier csv.

        Parameters:
        -----------
        user_name : str
            Nom de l'utilisateur à charger.

        Return:
        -------
        User or None
            Objet User représentant l'utilisateur chargé,
            ou None si l'utilisateur n'existe pas.
        """
        try:
            with open(self.file_path, 'r') as file:
                reader = csv.DictReader(file)
                for row in reader:
                    if row['name'] == user_name:
                        adult = row['adult']
                        if adult.lower()=='false':
                            adult = False
                        else:
                            adult = True
                        return User(int(row['id']), row['name'],
                                    row['password'],
                                    adult, row['language'],
                                    row['genre_serie'])
        except FileNotFoundError:
            print("Fichier introuvé.")
        return None

    def add_user(self, user_name, password, adult, language, genre_serie):
        """Ajout d'un utilisateur à la base de données.

        Parameters
        ----------
        user_name: str
            Nom de l'utilisateur.
        password: str
            Mot de passe de l'utilisateur.
        adult: bool
            Indicateur si l'utilisateur est adulte ou non.
        language: str
            Langue préférée de l'utilisateur.
        genre_serie: str
            Genre de série préféré de l'utilisateur.

        Return
        ------
        None
        """
        try:
            with open(self.file_path, 'r') as file:
                reader = csv.reader(file)
                next(reader, None)
                row = list(reader)
                if len(row) > 0:
                    last_user_id = max(int(element[0]) for element in row)
                    user_id = last_user_id + 1
                else:
                    user_id = 1
            with open(self.file_path, 'a', newline='') as file:
                writer = csv.writer(file)
                writer.writerow([user_id, user_name, password, adult,
                                 language, genre_serie])
            print("Utilisateur ajouté avec succès.")
        except Exception as e:
            print("Erreur:", e)

    def modify_user(self, user_name, new_name=None, new_password=None,
                      new_language=None, new_genre_serie=None):
        """Modification d'un utilisateur dans la base.

        Parameters
        ----------
        user_name: str
            Nom de l'utilisateur à modifier.
        new_name: str
            Nouveau nom de l'utilisateur (None par défaut).
        new_password: str
            Nouveau mot de passe (None par défaut).
        new_language: str
            Nouvelle langue de préférence (None par défaut).
        new_genre_serie: str
            Nouveau genre de série préféré (None par défaut).

        Returns
        -------
        None
        """
        try:
            with open(self.file_path, 'r') as file:
                reader = csv.reader(file)
                rows = list(reader)

            found = False
            with open(self.file_path, 'w', newline='') as file:
                writer = csv.writer(file)
                for row in rows:
                    if row[1] == user_name:
                        found = True
                        if new_name:
                            row[1] = new_name
                        if new_password:
                            row[2] = new_password
                        if new_language:
                            row[4] = new_language
                        if new_genre_serie:
                            row[5] = new_genre_serie
                        updated_user_info = row
                    writer.writerow(row)

            if found:
                print("Les informations utilisateur ont été mises à jour.")
                if updated_user_info:
                    return True
            else:
                print("Utilisateur introuvé.")

        except FileNotFoundError:
            print("Fichier introuvé.")

    def delete_user(self, user_name):
        """Suppression d'un utilisateur.

        Parameters:
        -----------
        user_name: str
            Nom de l'utilisateur à supprimer.

        Return:
        -------
        None
        """
        try:
            with open(self.file_path, 'r') as file:
                reader = csv.reader(file)
                rows = list(reader)

            found = False
            with open(self.file_path, 'w', newline='') as file:
                writer = csv.writer(file)
                for row in rows:
                    if row[1] != user_name:
                        writer.writerow(row)
                    else:
                        found = True
            if found:
                print("Utilisateur supprimé avec succès.")
            else:
                print("Utilisateur introuvé.")
        except FileNotFoundError:
            print("Fichier introuvé.")

    def verify_user(self, user_name):
        """Vérification de la présence d'un utilisateur dans la base.

        Parameters:
        -----------
        user_name: str
            Nom de l'utilisateur à supprimer.

        Return:
        -------
        bool
            True si l'utilisateur existe, False sinon
        """
        try:
            with open(self.file_path, 'r') as file:
                reader = csv.DictReader(file)
                for row in reader:
                    if row['name'] == user_name:
                        return True
        except FileNotFoundError:
            pass
        return False


if __name__ == "__main__":
    import doctest
    doctest.testmod(verbose=True)

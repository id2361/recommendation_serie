import pandas as pd
from serie import Serie


class UpcomingSeries:
    """Classe représentant une gestion des séries à venir.

    Cette classe permet de charger,
    ajouter, enregistrer et afficher les séries à venir.

    Attributes:
    -----------
    file_path : str
        Chemin d'accès au fichier CSV contenant les séries à venir.
    upcoming_series : list[Serie]
        Liste des objets Serie représentant
        les séries à venir chargées depuis le fichier CSV.

    Parameters:
    -----------
    file_path : str
        Chemin d'accès au fichier CSV contenant les séries à venir.

    Examples:
    ---------
    >>> import datetime
    >>> file_path = "upcoming_series.csv"
    >>> upcoming_series = UpcomingSeries(file_path)

    >>> print(upcoming_series.load_upcoming_series()[0])
    Nouvelle série
    >>> n=len(upcoming_series.upcoming_series)
    >>> nouvelle_serie = Serie(id_serie=7677, name="Nouvelle Série111",
    ...                         number_of_seasons=1, number_of_episodes=10,
    ...                         spoken_languages=["English"],
    ...                         first_air_date=datetime.datetime(2024, 5, 1),
    ...                         last_air_date=datetime.datetime(2024, 6, 1),
    ...                         status="In Production", genres=["Drama"],
    ...                         networks=["Network1"], adult=False)

    >>> upcoming_series.add_upcoming_series(nouvelle_serie)

    >>> len(upcoming_series.upcoming_series)== n+1
    True

    >>> upcoming_series.save_upcoming_series()
    """

    def __init__(self, file_path):
        self.file_path = file_path
        self.upcoming_series = self.load_upcoming_series()

    def load_upcoming_series(self):
        """Chargement de la liste des séries à venir à partir du fichier csv.

        Return:
        -------
        list[Serie]
            Liste d'objets de classe Serie. Liste contenant toutes les séries à
            venir.
        """
        try:
            data = pd.read_csv(self.file_path)
            upcoming_series = []
            for _, row in data.iterrows():
                new_serie = Serie(
                    id_serie=row['id'],
                    name=row['name'],
                    number_of_seasons=int(row['number_of_seasons']),
                    number_of_episodes=int(row['number_of_episodes']),
                    spoken_languages=row['spoken_languages'].split(','),
                    first_air_date=pd.to_datetime(row['first_air_date']),
                    last_air_date=pd.to_datetime(row['last_air_date']),
                    status=row['status'],
                    genres=row['genres'].split(','),
                    networks=row['networks'].split(','),
                    adult=bool(row['adult'])
                )
                upcoming_series.append(new_serie)
            return upcoming_series

        except FileNotFoundError:
            print(f"Le fichier {self.file_path} n'existe pas.")
            return []

    def add_upcoming_series(self, serie):
        """Ajout d'une série à venir à la liste.

        Parameters:
        -----------
        serie: Serie
            Série à ajouter à la liste des séries à venir.

        Return:
        -------
        None
        """
        self.upcoming_series.append(serie)

    def save_upcoming_series(self):
        """Enregistrement la liste des séries à venir dans le fichier csv.

        Return:
        -------
        None
        """
        data = {
            'id': [],
            'name': [],
            'number_of_seasons': [],
            'number_of_episodes': [],
            'spoken_languages': [],
            'first_air_date': [],
            'last_air_date': [],
            'status': [],
            'genres': [],
            'networks': [],
            'adult': []
        }
        for serie in self.upcoming_series:
            data['id'].append(serie.id_serie)
            data['name'].append(serie.name)
            data['number_of_seasons'].append(serie.number_of_seasons)
            data['number_of_episodes'].append(serie.number_of_episodes)
            data['spoken_languages'].append(','.join(serie.spoken_languages))
            data['first_air_date'].append(serie.first_air_date)
            data['last_air_date'].append(serie.last_air_date)
            data['status'].append(serie.status)
            data['genres'].append(','.join(serie.genres))
            data['networks'].append(','.join(serie.networks))
            data['adult'].append(1 if serie.adult else 0)

        df = pd.DataFrame(data)
        df.to_csv(self.file_path, index=False)

    def show_upcoming_series(self):
        """Afficher les séries à venir.

        Return:
        -------
        None
        """
        print("Séries à venir :")
        for serie in self.upcoming_series:
            print(f"- {serie.name}")


if __name__ == "__main__":
    import doctest
    doctest.testmod(verbose=True)

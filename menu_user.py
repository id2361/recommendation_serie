from genre import get_series_by_genre
from language import get_series_by_language
from classification import classification
from upcoming_series import UpcomingSeries
from featured_series import get_new_serie
from platform_series import get_series_by_platform


def second_menu_user(base_series):
    while True:
        print("\nMenu Utilisateur")
        print("1. Rechercher une série")
        print("2. Séries par Genre")
        print("3. Séries par Langue")
        print("4. Recommandations")
        print("5. Séries à Venir")
        print("6. Séries à la Une")
        print("7. Séries par plateforme de streaming")
        print("0. Quitter")

        choice = input("\nEntrez le numéro correspondant à votre choix : ")

        # Option : Série par Nom
        if choice == "1":
            title = input("Entrez le nom de la serie : ")
            serie = base_series.search_serie(title)
            if serie:
                print(serie, "est une série présente dans la base.")
            continue

        # Option : Séries par Genre
        elif choice == "2":
            print("\nChoisissez un genre :")
            print("0. Drama")
            print("1. Mystery")
            print("2. Action & Adventure")
            print("3. Family")
            print("4. Comedy")
            print("5. Crime")
            print("6. War & Politics")
            print("7. Soap")
            print("8. Documentary")
            print("9. Kids")
            while True:
                try:
                    choice1 = int(input("\nEntrez votre choix : "))
                    list_genre = ["Drama", "Mystery", "Action & Adventure",
                                "Family", "Comedy", "Crime", "War & Politics",
                                "Soap", "Documentary", "Kids"]
                    if choice1 in range(len(list_genre)):
                        get_series_by_genre(list_genre[choice1], base_series)
                        choice1 = None
                        break
                except:
                    print("Choix invalide. Veuillez entrer un genre "
                        "valide.")

        # Option : Séries par Langue
        elif choice == "3":
            print("Choisissez une langue :")
            print("0. Anglais")
            print("1. Français")
            print("2. Japonais")
            print("3. Coréen")
            print("4. Danois")
            print("5. Espagnol")
            print("6. Allemand")
            while True:
                try:
                    choice2 = int(input("\nEntrez votre choix : "))
                    liste2 = ["en", "fr", "ja", "ko", "da", "es", "de"]
                    if choice2 in range(len(liste2)):
                        get_series_by_language(liste2[choice2], base_series)
                        choice2 = None
                        break
                except:
                    print("Choix invalide. Veuillez entrer une langue valide "
                          "valide.")


        # Option : Recommandations
        elif choice == "4":
            while True:
                serie = input("\nTapez le titre d'une serie que vous avez "
                              "aimé : ")
                serie = base_series.search_serie(serie)
                if serie:
                    break

            while True:
                try :
                    n = int(input("Entrez le nombre de recommandaiton que vous"
                            " souhaitez (10 par défaut): "))
                    if n is None or n > 0:
                        break
                except:
                    print("Choix invalide. Veuillez rentrer un nombre valide "
                          "(> 0).")
            classification(base_series, serie.name, n)
            n = None
            pass

        elif choice == "5":
            upcoming_serie = UpcomingSeries("series_a_venir.csv")
            print(upcoming_serie.show_upcoming_series())
            pass

        elif choice == "6":
            get_new_serie(base_series)
            pass

        elif choice == "7":
            name = input("Saisissez le nom de votre plateforme : ")
            get_series_by_platform(name, base_series)

        # Option : Quitter
        elif choice == "0":
            print("Retour au menu d'authentification.")
            break

        else:
            print("Choix invalide. Veuillez entrer un numéro valide.\n")

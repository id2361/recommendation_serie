class Note:
    """Classe représentant les notes attribuées aux séries.

    Attributes:
    -----------
    notes: dict{name (str): notes (list[int])}
        Dictionnaire contenant en clé le nom d'une série, et en valeur une
        liste de notes attribuées à cette série.

    Examples:
    ---------
    >>> base_notes = Note()

    # Ajouter des notes pour différentes séries
    >>> base_notes.add_note("Game of Thrones", 4)
    >>> base_notes.add_note("Game of Thrones", 5)
    >>> base_notes.add_note("Stranger Things", 3)
    >>> base_notes.add_note("Stranger Things", 4)
    >>> base_notes.add_note("Stranger Things", 5)

    # Afficher les notes
    >>> base_notes.show_notes()
    Notes attribuées :
    - Game of Thrones : [4, 5] (Moyenne : 4.5)
    - Stranger Things : [3, 4, 5] (Moyenne : 4.0)

    # Calculer la moyenne des notes pour une série donnée
    >>> mean_game_of_thrones = base_notes.mean_notes("Game of Thrones")
    >>> if mean_game_of_thrones is not None:
    ...     print(f"Moyenne des notes pour 'Game of Thrones' : {mean_game_of_thrones}")
    Moyenne des notes pour 'Game of Thrones' : 4.5
    """

    def __init__(self):
        self.notes = {}

    def add_note(self, serie_name, note):
        """Ajout d'une note pour une série donnée.

        Parameters:
        -----------
        serie_name: str
            Nom de la série.
        note: int
            Note attribuée à la série.

        Return:
        -------
        None
        """
        if serie_name not in self.notes:
            self.notes[serie_name] = [note]
        else:
            self.notes[serie_name].append(note)

    def mean_notes(self, serie_name):
        """Calcul de la moyenne des notes pour une série donnée.

        Parameters:
        -----------
        serie_name: str
            Nom de la série.

        Return:
        -------
        None
        """
        if serie_name in self.notes:
            notes_serie = self.notes[serie_name]
            if notes_serie:
                return sum(notes_serie) / len(notes_serie)
            else:
                return 0
        else:
            print(f"Aucune note n'a été attribuée pour la série '{serie_name}'.")
            return None

    def show_notes(self):
        """Affichage des notes de toutes les séries.

        Return:
        -------
        None
        """
        print("Notes attribuées :")
        for serie_name, notes in self.notes.items():
            mean = self.mean_notes(serie_name)
            print(f"- {serie_name} : {notes} (Moyenne : {mean})")


if __name__ == "__main__":
    import doctest
    doctest.testmod(verbose=True)

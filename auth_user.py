from user_operation import register
from user_operation import connect
from menu_user import second_menu_user


def show_menu_user():
    print("\nQue souhaitez-vous faire ?")
    print("1. S'inscrire")
    print("2. Se connecter")
    print("3. Quitter")


def first_menu_user(base_series, base_user):
    while True:
        show_menu_user()
        choix = (input("Entrez votre choix : "))
        if choix == "1":
            register(base_user)
        elif choix == "2":
            connexion = connect(base_user)
            if connexion == False:
                continue
            second_menu_user(base_series)

        elif choix == "3":
            print("Retour au menu principal")
            break
        else:
            print("Choix invalide. Veuillez entrer un numéro valide.\n")

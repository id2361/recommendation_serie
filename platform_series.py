from serie import Serie
from base_series import BaseSeries


class Platform:
    """Classe représentant une plateforme de streaming.

    Attributes:
    -----------
    name : str
        Le nom de la plateforme.

    Examples:
    ---------
    >>> plat = Platform("Netflix")
    >>> plat.name
    'Netflix'
    >>> import datetime
    >>> serie2 = Serie(2, "Stranger Things", 3, 25, ["en"],
    ...                datetime.datetime(2016,7,15),
    ...                datetime.datetime(2019,7,4), "Ended",
    ...                ["Drama", "Fantasy", "Horror"],
    ...                ["Netflix"], False)
    >>> plat.check_platform_series(serie2)
    True
    >>> from base_serie import BaseSeries
    >>> base_series = BaseSeries()
    >>> base_series.add_serie(serie2)
    >>> get_series_by_platform("Netflix", base_series)
    Séries disponibles sur Netflix:
    Stranger Things

    """
    def __init__(self, name: str):
        if not isinstance(name, str):
            raise TypeError("Le nom de la plateforme doit être une instance "
                            "de str.")
        self.name = name

    def series_on_platform(self, base_series):
        """Récupération des séries disponibles sur une plateforme dans une
        base de données spécifiée.

        Parameters:
        -----------
        base_series: BaseSeries
            Base de données contenant les séries.

        Return:
        -------
        list
            Liste des séries présente sur la plateforme.
        """
        if not isinstance(base_series, BaseSeries):
            raise TypeError("La base conntenant des séries doit être une "
                            "instance de BaseSeries.")
        series_platform = []
        for serie in base_series.series.values():
            if self.name in serie.networks:
                series_platform.append(serie)
        return series_platform

    def check_platform_series(self, serie):
        """Vérification de la disponibilité d'une série est disponible sur une
        plateforme.

        Parameters:
        -----------
        serie: Serie
            Série à vérifier.

        Return:
        -------
        bool
            True si la série est présente sur la plateforme, False sinon.
        """
        if not isinstance(serie, Serie):
            raise TypeError("La série doit être une instance de Serie.")
        return self.name in serie.networks

    def __str__(self):
        return f"{self.name}"

def get_series_by_platform(platform_name, base_series):
    """Procédure qui affiche toutes les séries disponibles dans la base
    d'une plateforme de streaming.

    Parameters:
    -----------
    platform_name: str
        Nom de la plateforme.
    base_series: BaseSeries
        Base de données contenant les séries.
    """
    platform = Platform(platform_name)
    series = platform.series_on_platform(base_series)
    print(f"Séries disponibles sur {platform_name}:")
    for serie in series:
        print(serie.name)

if __name__ == "__main__":
    import doctest
    doctest.testmod(verbose=True)

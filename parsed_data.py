import datetime


def parse_data(id_serie, name,
               number_of_seasons, number_of_episodes,
               spoken_languages,
               first_air_date, last_air_date, status,
               genres, networks, adult):
    """Transformation des types des paramètres de la classe 'Serie'.

    Parameters:
    -----------
    id_serie: int
        ID de la série.
    name: str
        Nom de la série.
    number_of_seasons: int
        Nombre de saisons.
    number_of_episodes: int
        Nombre d'épisodes.
    spoken_languages: list[str]
        Langues parlées.
    first_air_date: datetime.datetime
        Date de première diffusion.
    last_air_date: datetime.datetime
        Date de dernière diffusion.
    status: str
        Statut de la série.
    genres: list[str]
        Genres de la série.
    networks: list[str]
        Réseaux de distribution de la série.
    adult: bool
        Indique si la série est destinée aux adultes.

    Return:
    -------
    tuple
        Tuple des paramètres dans le bon typage.

    Examples:
    ---------
    >>> serie = {"id_serie":"1",
    ...    "name":"Breaking Bad",
    ...    "number_of_seasons":"5",
    ...    "number_of_episodes":"62",
    ...    "spoken_languages":"fr, en",
    ...    "first_air_date":"2008-01-20",
    ...    "last_air_date":"2013-09-29",
    ...    "status":"Ended",
    ...    "genres":"Drama, Crime",
    ...    "networks":"AMC",
    ...    "adult":"False"}
    >>> serie = parse_data(
    ...    serie["id_serie"],
    ...    serie["name"],
    ...    serie["number_of_seasons"],
    ...    serie["number_of_episodes"],
    ...    serie["spoken_languages"],
    ...    serie["first_air_date"],
    ...    serie["last_air_date"],
    ...    serie["status"],
    ...    serie["genres"],
    ...    serie["networks"],
    ...    serie["adult"])
    >>> print(serie)
    (1, 'Breaking Bad', 5, 62, ['fr', 'en'], \
datetime.datetime(2008, 1, 20, 0, 0), \
datetime.datetime(2013, 9, 29, 0, 0), \
'Ended', ['Drama', 'Crime'], ['AMC'], True)
    """

    #id_serie, number_of_seasons et number_of_episodes doivent être de type int
    id_serie = int(id_serie)
    number_of_seasons = int(number_of_seasons)
    number_of_episodes = int(number_of_episodes)

    #name et status de type str
    name = str(name)
    status = str(status)

    #first_air_date et last_air_date de type datetime
    if isinstance(first_air_date, str):
        first_air_date = datetime.datetime.strptime(first_air_date, '%Y-%m-%d')
    else:
        first_air_date = datetime.datetime.min

    if isinstance(last_air_date, str):
        last_air_date = datetime.datetime.strptime(last_air_date, '%Y-%m-%d')
    else:
        last_air_date = datetime.datetime.min

    #genres, spoken_languages et networks de type list
    if isinstance(genres, str):
        genres = genres.split(', ')
    else:
        genres = []
    if isinstance(spoken_languages, str):
        spoken_languages = spoken_languages.split(', ')
    else:
        spoken_languages = []
    if isinstance(networks, str):
        networks = networks.split(', ')
    else:
        networks = []

   #adult de type booléen
    adult = bool(adult)
    return (id_serie, name, number_of_seasons, number_of_episodes,
            spoken_languages, first_air_date, last_air_date,
            status, genres, networks, adult)


if __name__ == "__main__":
    import doctest
    doctest.testmod(verbose=True)

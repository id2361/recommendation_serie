import pytest
import datetime
from serie import Serie
from base_series import BaseSeries
from platform_series import Platform

# Fixtures

@pytest.fixture
def base_series():
    # Créer une base de données de séries pour les tests
    base_series = BaseSeries()
    series = [
        Serie(1, "Breaking Bad", 5, 62, ["English"], datetime.datetime(2008, 1, 20), datetime.datetime(2013, 9, 29), "Ended", ["Drama", "Crime", "Thriller"], ["AMC"], False),
        Serie(2, "Game of Thrones", 8, 73, ["English"], datetime.datetime(2011, 4, 17), datetime.datetime(2019, 5, 19), "Ended", ["Action", "Adventure", "Drama", "Fantasy"], ["HBO"], True),
        Serie(3, "Stranger Things", 4, 34, ["English"], datetime.datetime(2016, 7, 15),datetime.datetime(2022, 7, 18), "In Production", ["Drama", "Fantasy", "Horror", "Mystery", "Sci-Fi", "Thriller"], ["Netflix"], False)
    ]
    for serie in series:
        base_series.add_serie(serie)
    return base_series

# Tests

def test_creation_platform(base_series):
    platform = Platform("Netflix")
    assert platform.name == "Netflix"

    series_netflix = platform.series_on_platform(base_series)
    assert len(series_netflix) == 1
    assert all(serie.name == "Stranger Things" for serie in series_netflix)

def test_check_platform_series():
    serie_netflix = Serie(1, "Stranger Things", 4, 34, ["English"], datetime.datetime(2016, 7, 15),datetime.datetime(2022, 7, 18), "In Production", ["Drama", "Fantasy", "Horror", "Mystery", "Sci-Fi", "Thriller"], ["Netflix"], False)
    serie_amc = Serie(2, "Breaking Bad", 5, 62, ["English"], datetime.datetime(2008, 1, 20), datetime.datetime(2013, 9, 29), "Ended", ["Drama", "Crime", "Thriller"], ["AMC"], False)
    platform_netflix = Platform("Netflix")
    platform_amc = Platform("AMC")
    assert platform_netflix.check_platform_series(serie_netflix) == True
    assert platform_amc.check_platform_series(serie_netflix) == False
    assert platform_amc.check_platform_series(serie_amc) == True

from admin import Admin
from menu_admin import menu_admin


def auth_admin(base_series):
    """Procédure d'authentification de l'administrateur.

    Parameters:
    -----------
    base_series: BaseSeries
        Base de données contenant les séries.
    """
    admin = Admin()
    admin.login_page()
    if admin.connected:
        menu_admin(base_series)
    else:
        pass

from base_user import BaseUser


def register(base_user: BaseUser):
    while True:
        user_name = input("Entrez votre nom d'utilisateur : ")
        if not user_name.replace(" ",""):
            print("Veuillez entre des caractères pour votre nom "
                  "d'utilisateur.")
        else:
            break

    while True:
        password = input("Entrez votre mot de passe : ")
        if not password.replace(" ",""):
            print("Veuillez entre des caractères pour votre mot de passe.")
        else:
            break
    adult = input(
        "Êtes-vous un adulte? (Oui/Non)(par défaut : Non): ").lower() == "oui"
    language = input(
        "Choisissez votre langue (par défaut : Anglais) : ") or "Anglais"
    genre_serie = input(
        "Quel est votre genre de série préféré? (par défaut : Aucun) : ") or \
            "Aucun"
    if not user_name:
        print("Le nom de l'utilisateur est vide.")

    # Vérifier si l'utilisateur existe déjà
    if base_user.verify_user(user_name):
        print("L'utilisateur existe déjà.")
        return

    # Ajouter l'utilisateur
    base_user.add_user(user_name, password, adult, language, genre_serie)
    print("Inscription réussie.")


def connect(base_user):
    while True:
        if not isinstance(base_user, BaseUser):
            raise TypeError("base_user doit être une instance de BaseUser.")
        user_name = input("Entrez votre nom d'utilisateur : ")
        password = input("Entrez votre mot de passe : ")

        user = base_user.load_user(user_name)
        if user and user.password == password:
            print("Connexion réussie.")
            return True
        else:
            print("Nom d'utilisateur ou mot de passe incorrect.")
            retry = input("Voulez-vous réessayez la connexion "
                          "(0 : non, 1: oui) ? ")
            if int(retry) == 0:
                return False


# Mettre à jour les préférences de l'utilisateur
def manage_account(base_user: BaseUser, user_name):
    new_name = input("Saisissez votre nouveau nom (actuelle: {}): ") or None
    new_password = input(
        "Saisissez votre nouveau mot de passe (actuelle: {}): ") or None
    new_language = input(
        "Choisissez votre nouvelle langue (actuelle: {}): ") or None
    new_genre_serie = input(
        "Choisissez votre nouveau genre de série préféré (actuel: {}): \
            ") or None

    if new_name and base_user.verify_user(new_name):
        print(
            "Le nouveau nom d'utilisateur existe déjà. Veuillez choisir un "
            "autre nom."
        )
        return

    if new_name or new_password or new_language or new_genre_serie:
        updated_user = base_user.modify_user(
            user_name,
            new_nom=new_name, new_mot_de_passe=new_password,
            new_langue=new_language, new_genre_serie=new_genre_serie
        )
        print("Préférences mises à jour avec succès.")
        return updated_user
    else:
        print("Aucune nouvelle préférence spécifiée.")
        user = base_user.load_user(user_name)
        return user


def supprimer_compte(base_user: BaseUser, user_name):
    confirmation = input(
        "Êtes-vous sûr de vouloir supprimer votre compte? (Oui/Non): ").lower()
    if confirmation == "oui":
        base_user.delete_user(user_name)
        print("Compte supprimé avec succès.")
    else:
        print("Suppression de compte annulée.")

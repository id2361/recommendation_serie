class User():
    """Classe représentant un utilisateur.

    Attributes:
    -----------
    user_id : int
        Identifiant de l'utilisateur.
    user_name : str
        Nom de l'utilisateur.
    password : str
        Mot de passe de l'utilisateur (par défaut 123456).
    adult : bool
        True si l'utilisateur est majeur (+18 ans).
    language : str
        Langue préférée de l'utilisateur.
    genre_serie : str
        Genre de série préféré de l'utilisateur.

    Parameters:
    -----------
    user_id : int
        Identifiant de l'utilisateur.
    user_name : str
        Nom de l'utilisateur.
     : str
        Mot de passe de l'utilisateur (par défaut 123456).
    adult : bool
        True si l'utilisateur est majeur (+18 ans).
    language : str
        Langue préférée de l'utilisateur.
    genre_serie : str
        Genre de série préféré de l'utilisateur.

    Examples:
    ---------
    >>> user = User(2, "Uzi", "123456", False, "Chinois", "Action")
    >>> print(user)
    2, Uzi, False, Chinois, Action
    """
    def __init__(self, user_id, user_name, password,
                 adult, language, genre_serie):
        if not isinstance(user_id, int):
            raise TypeError("L'ID de l'utilisateur doit être une instance de "
                            "int.")
        if not isinstance(user_name, str):
            raise TypeError(
                "Le nom de l'utilisateur doit être une instance de str.")
        if not isinstance(password, str):
            raise TypeError(
                "Le mot de passe doit être une instance de str.")
        if not isinstance(adult, bool):
            raise TypeError(
                "L'option adulte doit être une instance de bool.")
        if not isinstance(language, str):
            raise TypeError(
                "La langue doit être une instance de str.")
        if not isinstance(genre_serie, str):
            raise TypeError(
                "Le genre de séries doit être une instance de str.")
        self.user_id = user_id
        self.user_name = user_name
        self.password = password
        self.adult = adult
        self.language = language
        self.genre_serie = genre_serie

    def __str__(self):
        return f"{self.user_id}, {self.user_name}, {self.adult}, " \
            f"{self.language}, {self.genre_serie}"


if __name__ == "__main__":
    import doctest
    doctest.testmod(verbose=True)

from base_series import BaseSeries
from base_user import BaseUser
from auth_user import first_menu_user
from auth_admin import auth_admin


def show_main_menu():
    print("\nVeuillez choisir le type d'utilisateur :")
    print("1. Utilisateur")
    print("2. Administrateur")
    print("3. Quitter")


def main_menu():
    """Fonction principale pour exécuter le menu général de l'application.

    """
    print("\nBienvenue !\n")

    base_series = BaseSeries()

    base_series.load_from_path("dat_test.csv")
    base_user = BaseUser("data_users.csv")
    while True:
        show_main_menu()
        choix = input("\nEntrez le numéro correspondant à votre choix : ")

        if choix == "1":  # Choix du menu utilisateur.
            first_menu_user(base_series, base_user)

        elif choix == "2":  # Choix du menu administrateur.
            auth_admin(base_series)

        elif choix == "3":  # Choix de quitter l'application.
            print("Merci d'avoir utilisé notre application. "
                  "Nous avons hâte de vous revoir. À bientôt !")
            break
        else:
            print("Choix invalide. Veuillez entrer un numéro valide.\n")


if __name__ == "__main__":
    main_menu()

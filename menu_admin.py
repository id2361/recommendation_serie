from serie import Serie
from base_series import BaseSeries
import pandas as pd
import datetime

def menu_admin(base_series):
    """Procédure qui affiche le menu administrateur et ses différentes
    fonctionnalités.
    """
    while True:
        print("\nBienvenue dans le menu administrateur :")
        print("1. Chercher une série")
        print("2. Modifier une série existante")
        print("3. Ajouter une nouvelle série")
        print("4. Supprimer une série")
        print("5. Quitter")

        choice = input("Entrez votre choix : ")

        if choice == '1':
            title = input("Entrez le nom de la serie : ")
            serie = base_series.search_serie(title)
            if serie:
                print(serie, "est une série présente dans la base.")
            continue

        elif choice == '2':
            while True:
                try:
                    id_serie = int(input("Entrez l'ID de la série à "
                                         "modifier : "))
                except:
                    print("Veuillez entrer un identifiant de type int.")
                    continue

                if id_serie == 456:
                    print("Pour le bon fonctionnement de cette application, "
                          "cet id ne peut etre modifier.")

                list_attributes = ["name", "number_of_seasons",
                                   "number_of_episodes", "spoken_languages",
                                   "first_air_date", "last_air_date",
                                   "status", "genres", "networks", "adult"]

                while True:
                    attribute = input("Entrez le nom de l'attribut à "
                                      "modifier : ")
                    if attribute not in list_attributes:
                        print("Le nom de l'attribut n'existe pas. "
                              "Pour rappel, voici la liste des attributs "
                              "des objets de classe Serie :")
                        for element in list_attributes:
                            print("- ", element)
                        continue
                    break

                while True:
                    new_value = input(f"Entrez la nouvelle valeur pour "
                        f"l'attribut '{attribute}' : ")
                    if attribute in ["name", "status"]:
                        try:
                            new_value = str(new_value)
                        except:
                            print("La valeur à modifier doit être une instance"
                                  " de str pour les attributs 'name' et "
                                  "'status'.")
                            continue
                    elif attribute in ["number_of_seasons",
                                     "number_of_episodes"]:
                        try:
                            new_value = int(new_value)
                        except:
                            print("La valeur à modifier doit être une instance"
                                  " de int pour les attributs "
                                  "'number_of_seasons' et "
                                  "'number_of_episodes'.")
                            continue
                    elif attribute == "adult":
                        try:
                            if new_value.lower()=='true':
                                new_value = True
                            elif new_value.lower() =='false':
                                new_value = False
                        except:
                            print("La valeur à modifier doit être une instance"
                                  " de booléen pour les attributs 'adult'.")
                            continue
                    elif attribute in ["spoken_languages", "genres",
                                       "networks"]:
                        try:
                            new_value = new_value.replace(" ", "").split(",")
                        except:
                            print("La valeur à modifier doit être sous le "
                                  "format suivant :\n"
                                  "element1, element2, element3, ...\n"
                                  "pour les attributs 'spoken_languages',"
                                  "'genres' et 'networks'.")
                            continue
                    else:
                        try:
                            new_value = datetime.datetime.strptime(new_value,
                                                                   '%Y-%m-%d')
                        except:
                            print("La valeur à modifier doit être sous le "
                                  "format suivant:\n"
                                  "yyyy-mm-dd\n"
                                  "pour les attributs 'first_air_date' et"
                                  "'last_air_date'.")
                            continue
                    break

                setattr(base_series.series.get(id_serie), attribute, new_value)
                print(f"La série à l'identifiant '{id_serie}' a bien été "
                      f"modifiée. Nouvelle série :\n"
                      f"- {base_series.series.get(id_serie).name}\n"
                      f"- {base_series.series.get(id_serie).number_of_seasons}"
                      f"\n- "
                      f"{base_series.series.get(id_serie).number_of_episodes}"
                      f"\n- "
                      f"{base_series.series.get(id_serie).spoken_languages}\n"
                      f"- {base_series.series.get(id_serie).first_air_date}\n"
                      f"- {base_series.series.get(id_serie).last_air_date}\n"
                      f"- {base_series.series.get(id_serie).status}\n"
                      f"- {base_series.series.get(id_serie).genres}\n"
                      f"- {base_series.series.get(id_serie).networks}\n"
                      f"- {base_series.series.get(id_serie).adult}\n")
                break



        # Option : Ajouter une nouvelle série
        elif choice == '3':
            print("Ajout d'une nouvelle série : ")

            while True:
                name = input("Entrez le nom de la nouvelle série : ")
                if not name.replace(" ", ""):
                    print("Veuillez précisez le nom de la nouvelle série.")
                    continue
                elif base_series.search_serie(name):
                    print(f"La série {name} existe déjà.")
                    continue
                break
            print("Nous pouvons donc continuer.\n")

            list_language = ["en", "fr", "ja", "ko", "da", "es", "de"]
            while True:
                spoken_languages = input("Entrez les langues (séparées par "
                                         "des virgules) : "
                                         "").replace(" ", "").split(',')

                stop = False
                for spoken_language in spoken_languages :
                    if spoken_language not in list_language:
                        print(f"Veuillez écrire des langues parmi la liste "
                            f"suivante :\n {', '.join(list_language)}")
                        stop = True
                        break
                if stop == True:
                    continue
                break

            list_status = ["In Production", "Pilot", "Planned",
                           "Returning Series", "Ended", "Canceled"]
            while True:
                status = input("Entrez le statut de la série : ")

                if status not in list_status:
                    print(f"Veuillez écrire un statut parmi la liste "
                          f"suivante :\n {', '.join(list_status)}")
                    continue
                break

            list_genres = ["Drama", "Mystery", "Action & Adventure", "Family",
                           "Comedy", "Crime", "War & Politics", "Soap",
                           "Documentary", "Kids"]

            while True:
                genres = input(
                    "Entrez les genres (séparés par des virgules) : "
                    ).replace(" ", "").split(',')
                if not genres:
                    print(f"Veuillez écrire un genre parmi la liste "
                          f"suivante :\n {', '.join(list_genres)}")
                    continue

                stop = False
                for genre in genres:
                    if genre not in list_genres:
                        print(f"Veuillez écrire un genre parmi la liste "
                              f"suivante :\n {', '.join(list_genres)}")
                        stop = True
                        break
                if stop == True:
                    continue
                break

            while True:
                networks = input("Entrez les réseaux de distribution"
                    "(séparés par des virgules) : ").replace(" ",
                                                             "").split(',')
                if not networks:
                    print(f"Veuillez écrire un réseau de distribution.")
                    continue
                break

            while True:
                try:
                    id_serie = int(input("Entrez l'ID de la nouvelle "
                                         "série : "))
                except:
                    print("L'identifiant de la série doit être une instance de"
                          "int.")
                    continue
                break

            while True:
                try:
                    number_of_seasons = int(input("Entrez le nombre de "
                                                  "saisons : "))
                except:
                    print("Le nombre de saisons doit être une instance de"
                          "int.")
                    continue
                break

            while True:
                try:
                    number_of_episodes = int(input("Entrez le nombre "
                                                   "d'épisodes : "))
                except:
                    print("Le nombre d'épisodes doit être une instance de "
                          "int.")
                    continue
                break

            while True:
                try:
                    first_air_date = pd.to_datetime(input(
                        "Entrez la date de première diffusion (AAAA-MM-JJ) : "
                    ), format="%Y-%m-%d")
                except:
                    print("La première date de diffusion n'est pas dans "
                          "le bon format. Faites comme suit :\n"
                          "AAAA-MM-JJ")
                    continue
                break

            while True:
                try:
                    last_air_date = pd.to_datetime(input(
                        "Entrez la date de dernière diffusion (AAAA-MM-JJ) : "
                    ), format='%Y-%m-%d')
                except:
                    print("La dernière date de diffusion n'est pas dans "
                          "le bon format. Faites comme suit :\n"
                          "AAAA-MM-JJ")
                    continue
                break

            while True:
                adult = input(
                    "La série est-elle destinée aux adultes ? (True/False) : "
                    ).lower()
                if adult == 'true':
                    adult = True
                    break
                elif adult == 'false':
                    adult = False
                    break
                else:
                    print("Veuillez entrer une 'True' ou 'False' pour savoir"
                        "si vous êtes majeurs.")


            new_serie = Serie(
                id_serie=id_serie,
                name=name,
                number_of_seasons=number_of_seasons,
                number_of_episodes=number_of_episodes,
                spoken_languages=spoken_languages,
                first_air_date=first_air_date,
                last_air_date=last_air_date,
                status=status,
                genres=genres,
                networks=networks,
                adult=adult
            )

            base_series.add_serie(new_serie)

        # Option : Supprimer une série
        elif choice == '4':
            serie_id = int(input("Entrez l'ID de la série à supprimer : "))
            base_series.delete_serie(serie_id)

        # Option : Quitter
        elif choice == '5':
            print("Merci d'avoir utilisé le menu administrateur. Au revoir !")
            break
        else:
            print("Choix invalide. Veuillez sélectionner une option valide.")

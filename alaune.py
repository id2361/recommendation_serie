import datetime
from base_serie import BaseSeries


class AlaUne:
    """Classe représentant une série inédite.

    Attributes:
    -----------
    base_series : BaseSeries
        Base de données contenant les sédzadazdazdaries

    Parameters:
    -----------
    base_series : BaseSeries
        Base de données contenant les séries

    Examples:
    ---------
    >>> base_series1 = BaseSeries()
    >>> base_series1.load_from_path("serie_test.csv")
    Série Game of Thrones ajoutée à la base de données.
    Série Money Heist ajoutée à la base de données.
    Série Stranger Things ajoutée à la base de données.
    Série The Walking Dead ajoutée à la base de données.
    Série Lucifer ajoutée à la base de données.
    Série Riverdale ajoutée à la base de données.
    Série Squid Game ajoutée à la base de données.
    Série Breaking Bad ajoutée à la base de données.
    Série The Good Doctor ajoutée à la base de données.
    Série WandaVision ajoutée à la base de données.
    Série The Flash ajoutée à la base de données.
    Série The Big Bang Theory ajoutée à la base de données.
    Série Loki ajoutée à la base de données.
    Série Grey's Anatomy ajoutée à la base de données.
    Série The Mandalorian ajoutée à la base de données.
    Série The Simpsons ajoutée à la base de données.
    Série Euphoria ajoutée à la base de données.
    Série The Umbrella Academy ajoutée à la base de données.
    Série Peaky Blinders ajoutée à la base de données.
    Base de données chargée avec succès depuis serie_test.csv.
    >>> get_new_serie(base_series1)
    Séries récentes (sorties il y a moins d'un an) :
    - Riverdale
    - The Flash
    - Grey's Anatomy
    - The Simpsons

    """
    def __init__(self, base_series):
        if not isinstance(base_series, BaseSeries):
            raise TypeError("base_series doit être une instance de "
                            "BaseSeries.")
        self.base_series = base_series

    def series_sorties_moins_un_an(self):
        series_sorties = []
        date_actuelle = datetime.datetime.now()

        for serie in self.base_series.series.values():
            if (date_actuelle - serie.last_air_date).days <= 365:
                series_sorties.append(serie)

        return series_sorties


def get_new_serie(base_series):
    a_la_une = AlaUne(base_series)
    series_recentes = a_la_une.series_sorties_moins_un_an()
    print("Séries récentes (sorties il y a moins d'un an) :")
    for serie in series_recentes:
        print(f"- {serie.name}")


if __name__ == "__main__":
    import doctest
    doctest.testmod(verbose=True)

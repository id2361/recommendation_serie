import tkinter as tk
from tkinter import messagebox
from base_series import BaseSeries
from base_user import BaseUser
from auth_user import first_menu_user
from auth_admin import auth_admin

# Définition de la fonction pour afficher le menu général
def afficher_menu_general():
    label_menu.config(text="Bienvenue !\nVeuillez choisir le type d'utilisateur :")
    bouton_utilisateur.config(text="Utilisateur", command=ouvrir_menu_utilisateur)
    bouton_administrateur.config(text="Administrateur", command=ouvrir_menu_administrateur)
    bouton_quitter.config(text="Quitter", command=fenetre.quit)

# Définition des fonctions pour ouvrir les menus utilisateur et administrateur
def ouvrir_menu_utilisateur():
    # Ajoutez ici le code pour gérer le menu utilisateur
    base_series = BaseSeries()
    base_series.load_from_path("dat_test.csv")
    base_user = BaseUser()
    first_menu_user(base_series, base_user)

def ouvrir_menu_administrateur():
    # Ajoutez ici le code pour gérer le menu administrateur
    base_series = BaseSeries()
    base_series.load_from_path("dat_test.csv")
    auth_admin(base_series)

# Création de la fenêtre principale
fenetre = tk.Tk()
fenetre.title("Application de Recommandation de Séries")

# Création des widgets pour afficher le menu général
label_menu = tk.Label(fenetre, text="")
label_menu.pack()

bouton_utilisateur = tk.Button(fenetre, text="", command=None)
bouton_utilisateur.pack()

bouton_administrateur = tk.Button(fenetre, text="", command=None)
bouton_administrateur.pack()

bouton_quitter = tk.Button(fenetre, text="", command=None)
bouton_quitter.pack()

# Affichage initial du menu général
afficher_menu_general()

# Lancement de la boucle principale de l'interface Tkinter
fenetre.mainloop()

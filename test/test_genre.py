import pytest
import datetime
from genre import Genre
from serie import Serie
from base_series import BaseSeries

# Fixtures

@pytest.fixture
def base_series():
    # Créer une base de données de séries pour les tests
    base_series = BaseSeries()
    series = [
        Serie(1, "Breaking Bad", 5, 62, ["English"], datetime.datetime(2008, 1, 20), datetime.datetime(2013, 9, 29), "Ended", ["Drama", "Crime", "Thriller"], ["AMC"], False),
        Serie(2, "Game of Thrones", 8, 73, ["English"], datetime.datetime(2011, 4, 17), datetime.datetime(2019, 5, 19), "Ended", ["Action", "Adventure", "Drama", "Fantasy"], ["HBO"], True),
        Serie(3, "Stranger Things", 4, 34, ["English"], datetime.datetime(2016, 7, 15), datetime.datetime(2023, 6, 11), "In Production", ["Drama", "Fantasy", "Horror", "Mystery", "Sci-Fi", "Thriller"], ["Netflix"], False)
    ]
    for serie in series:
        base_series.add_serie(serie)
    return base_series

# Tests

def test_creation_genre():
    genre = Genre("Drama")
    assert genre.name == "Drama"

def test_series_of_genre(base_series):
    genre = Genre("Drama")
    series_drama = genre.series_of_genre(base_series)
    assert len(series_drama) == 3
    assert all(serie.name in ["Breaking Bad", "Stranger Things", "Game of Thrones"] for serie in series_drama)

def test_verify_serie_genre():
    serie = Serie(1, "Breaking Bad", 5, 62, ["English"], datetime.datetime(2008, 1, 20), datetime.datetime(2013, 9, 29), "Ended", ["Drama", "Crime", "Thriller"], ["AMC"], False)
    genre = Genre("Drama")
    assert genre.verify_serie_genre(serie) == True
    genre = Genre("Comedy")
    assert genre.verify_serie_genre(serie) == False

import datetime
import pytest
from serie import Serie


def test_serie_initialization():
    # Test des types de données valides
    s = Serie(1, "Breaking Bad", 5, 62, ["English"],
              datetime.datetime(2008, 1, 20), datetime.datetime(2013, 9, 29),
              "Ended", ["Drama", "Crime"], ["AMC"], False)
    assert isinstance(s, Serie)


def test_serie_invalid_initialization():
    # Test des types de données invalides
    with pytest.raises(TypeError):
        # ID de série doit être un entier
        Serie("1", "Breaking Bad", 5, 62, ["English"],
              datetime.datetime(2008, 1, 20), datetime.datetime(2013, 9, 29),
              "Ended", ["Drama", "Crime"], ["AMC"], False)

    with pytest.raises(TypeError):
        # Titre de série doit être une chaîne de caractères
        Serie(1, 123, 5, 62, ["English"],
              datetime.datetime(2008, 1, 20), datetime.datetime(2013, 9, 29),
              "Ended", ["Drama", "Crime"], ["AMC"], False)

    # Ajoutez d'autres tests pour les autres types de données invalides


def test_invalid_name_type():
    # Test d'initialisation avec un type incorrect pour le nom de série
    with pytest.raises(TypeError):
        Serie(1, 123, 5, 62, ["English"], 
              datetime.datetime(2008, 1, 20), datetime.datetime(2013, 9, 29),
              "Ended", ["Drama", "Crime"], ["AMC"], False)


def test_serie_str():
    # Test de la méthode __str__
    s = Serie(1, "Breaking Bad", 5, 62, ["English"],
              datetime.datetime(2008, 1, 20), datetime.datetime(2013, 9, 29),
              "Ended", ["Drama", "Crime"], ["AMC"], False)
    assert str(s) == "Breaking Bad"

from note import Note

# Tests


def test_add_note():
    base_notes = Note()
    base_notes.add_note("Game of Thrones", 4)
    base_notes.add_note("Game of Thrones", 5)
    assert base_notes.notes == {"Game of Thrones": [4, 5]}


def test_mean_notes():
    base_notes = Note()
    base_notes.add_note("Game of Thrones", 4)
    base_notes.add_note("Game of Thrones", 5)
    moyenne_game_of_thrones = base_notes.mean_notes("Game of Thrones")
    assert moyenne_game_of_thrones == 4.5


def test_check_platform_series_serie_non_existent():
    base_notes = Note()
    non_existent_series_mean = base_notes.mean_notes("Série Inexistante")
    assert non_existent_series_mean is None


def test_show_notes(capsys):
    base_notes = Note()
    base_notes.add_note("Game of Thrones", 4)
    base_notes.add_note("Game of Thrones", 5)
    base_notes.add_note("Stranger Things", 3)
    base_notes.add_note("Stranger Things", 4)
    base_notes.add_note("Stranger Things", 5)

    base_notes.show_notes()
    captured = capsys.readouterr()
    assert "Notes attribuées :" in captured.out
    assert "- Game of Thrones : [4, 5] (Moyenne : 4.5)" in captured.out
    assert "- Stranger Things : [3, 4, 5] (Moyenne : 4.0)" in captured.out

import pytest
from user import User
from serie import Serie
import datetime
from already_watched import AlreadyWatched

# Tests

def test_creation_already_watch():
    user = User(1, "John", "password", True, "English", "Action")
    already_watch = AlreadyWatched(user)
    assert already_watch.user == user
    assert already_watch.series_watched == []

def test_add_serie_watched():
    user = User(1, "John", "password", True, "English", "Action")
    already_watch = AlreadyWatched(user)
    serie = Serie(1, "Breaking Bad", 5, 62, ["English"], datetime.datetime(2008,1,20),datetime.datetime(2013,9,29), "Ended", ["Drama"], ["AMC"], False)
    already_watch.add_serie_watched(serie)
    assert already_watch.series_watched == [serie]

def test_supprimer_serie():
    user = User(1, "John", "password", True, "English", "Action")
    already_watch = AlreadyWatched(user)
    serie = Serie(1, "Breaking Bad", 5, 62, ["English"], datetime.datetime(2008,1,20),datetime.datetime(2013,9,29), "Ended", ["Drama"], ["AMC"], False)
    already_watch.add_serie_watched(serie)
    assert already_watch.series_watched == [serie]
    already_watch.delete_serie_watched(serie)
    assert already_watch.series_watched == []

def test_ajouter_serie_mauvais_type():
    user = User(1, "John", "password", True, "English", "Action")
    already_watch = AlreadyWatched(user)
    with pytest.raises(TypeError):
        already_watch.add_serie_watched("Breaking Bad")

def test_delete_serie_watched_bad_type():
    user = User(1, "John", "password", True, "English", "Action")
    already_watch = AlreadyWatched(user)
    with pytest.raises(TypeError):
        already_watch.delete_serie_watched("Breaking Bad")

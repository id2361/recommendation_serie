import pytest
from datetime import datetime
from base_series import BaseSeries
from serie import Serie


@pytest.fixture
def base_series():
    return BaseSeries()


def test_load_from_path(base_series):
    file_name = "C:/Users/Céleste/Desktop/1A INGE/projet_info/recommendation_serie/test/test_data_serie.csv"
    base_series.load_from_path(file_name)
    # Vérifier que la base de données contient des séries après le chargement
    assert len(base_series.series) > 0


def test_add_serie(base_series):
    # Créer une série pour ajouter à la base de données
    new_serie = Serie(
        id_serie=1,
        name="Test Serie",
        number_of_seasons=3,
        number_of_episodes=24,
        spoken_languages=["English", "French"],
        first_air_date=datetime(2022, 1, 1),
        last_air_date=datetime(2023, 1, 1),
        status="Ongoing",
        genres=["Action", "Drama"],
        networks=["Netflix"],
        adult=False
    )
    # Ajouter la série à la base de données
    base_series.add_serie(new_serie)
    # Vérifier que la série a bien été ajoutée
    assert new_serie.id_serie in base_series.series


def test_delete_serie(base_series):
    # Ajouter une série factice pour la supprimer ensuite
    new_serie = Serie(
        id_serie=1,
        name="Test Serie",
        number_of_seasons=3,
        number_of_episodes=24,
        spoken_languages=["English", "French"],
        first_air_date=datetime(2022, 1, 1),
        last_air_date=datetime(2023, 1, 1),
        status="Ongoing",
        genres=["Action", "Drama"],
        networks=["Netflix"],
        adult=False
    )
    base_series.add_serie(new_serie)
    # Supprimer la série de la base de données
    base_series.delete_serie(1)
    # Vérifier que la série a bien été supprimée
    assert 1 not in base_series.series


def test_search_serie(base_series):
    # Créer une série factice et l'ajouter à la base de données
    new_serie = Serie(
        id_serie=1,
        name="Test Serie",
        number_of_seasons=3,
        number_of_episodes=24,
        spoken_languages=["English", "French"],
        first_air_date=datetime(2022, 1, 1),
        last_air_date=datetime(2023, 1, 1),
        status="Ongoing",
        genres=["Action", "Drama"],
        networks=["Netflix"],
        adult=False
    )
    base_series.add_serie(new_serie)
    # Rechercher la série par son titre
    found_serie = base_series.search_serie("Test Serie")
    # Vérifier que la série retournée est celle ajoutée
    assert found_serie == new_serie
    # Tester la recherche avec un titre inexistant
    #with pytest.raises(KeyError):
        #base_series.search_serie("Unknown Serie")


if __name__ == "__main__":
    pytest.main()

import pytest
import datetime
from serie import Serie
from base_serie import BaseSeries
from plateforme import PlateForme

# Fixtures

@pytest.fixture
def base_series():
    # Créer une base de données de séries pour les tests
    base_series = BaseSeries()
    series = [
        Serie(1, "Breaking Bad", 5, 62, ["English"], datetime.datetime(2008, 1, 20), datetime.datetime(2013, 9, 29), "Ended", ["Drama", "Crime", "Thriller"], ["AMC"], False),
        Serie(2, "Game of Thrones", 8, 73, ["English"], datetime.datetime(2011, 4, 17), datetime.datetime(2019, 5, 19), "Ended", ["Action", "Adventure", "Drama", "Fantasy"], ["HBO"], True),
        Serie(3, "Stranger Things", 4, 34, ["English"], datetime.datetime(2016, 7, 15),datetime.datetime(2022, 7, 18), "In Production", ["Drama", "Fantasy", "Horror", "Mystery", "Sci-Fi", "Thriller"], ["Netflix"], False)
    ]
    for serie in series:
        base_series.add_serie(serie)
    return base_series

# Tests

def test_creation_plateforme():
    plateforme = PlateForme("Netflix")
    assert plateforme.name == "Netflix"

def test_series_sur_plateforme(base_series):
    plateforme = PlateForme("Netflix")
    series_netflix = plateforme.series_sur_plateforme(base_series)
    assert len(series_netflix) == 1
    assert all(serie.name == "Stranger Things" for serie in series_netflix)

def test_verifier_serie_plateforme():
    serie_netflix = Serie(1, "Stranger Things", 4, 34, ["English"], datetime.datetime(2016, 7, 15),datetime.datetime(2022, 7, 18), "In Production", ["Drama", "Fantasy", "Horror", "Mystery", "Sci-Fi", "Thriller"], ["Netflix"], False)
    serie_amc = Serie(2, "Breaking Bad", 5, 62, ["English"], datetime.datetime(2008, 1, 20), datetime.datetime(2013, 9, 29), "Ended", ["Drama", "Crime", "Thriller"], ["AMC"], False)
    plateforme_netflix = PlateForme("Netflix")
    plateforme_amc = PlateForme("AMC")
    assert plateforme_netflix.verifier_serie_plateforme(serie_netflix) == True
    assert plateforme_amc.verifier_serie_plateforme(serie_netflix) == False
    assert plateforme_amc.verifier_serie_plateforme(serie_amc) == True

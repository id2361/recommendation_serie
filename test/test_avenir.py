import datetime
import pandas as pd
import pytest
from upcoming_series import UpcomingSeries
from serie import Serie

# Fixtures


@pytest.fixture
def file_path(tmp_path):
    file_path = tmp_path / "test_serie_avenir.csv"
    return str(file_path)

# Tests
def test_avoir_chemin_acces(file_path):
    upcoming_series = UpcomingSeries(file_path)
    assert upcoming_series.file_path == file_path


def test_load_upcoming_series(file_path):
    data = {
        'id': [1, 2],
        'name': ['Série1', 'Série2'],
        'number_of_seasons': [5, 3],
        'number_of_episodes': [50, 30],
        'spoken_languages': ['French,English', 'Spanish'],
        'first_air_date': ['2024-01-01', '2024-02-01'],
        'last_air_date': ['2024-12-31', '2024-04-30'],
        'status': ['In Production', 'Ended'],
        'genres': ['Drama,Comedy', 'Action'],
        'networks': ['Network1,Network2', 'Network3'],
        'adult': [0, 1]
    }
    df = pd.DataFrame(data)
    df.to_csv(file_path, index=False)

    upcoming_series = UpcomingSeries(file_path)
    series = upcoming_series.load_upcoming_series()

    assert len(series) == 2
    assert isinstance(series[0], Serie)
    assert series[0].name == 'Série1'
    assert series[0].number_of_seasons == 5
    assert series[0].number_of_episodes == 50
    assert series[0].spoken_languages == ['French', 'English']
    assert series[0].first_air_date == datetime.datetime(2024, 1, 1)
    assert series[0].last_air_date == datetime.datetime(2024, 12, 31)
    assert series[0].status == 'In Production'
    assert series[0].genres == ['Drama', 'Comedy']
    assert series[0].networks == ['Network1', 'Network2']
    assert not series[0].adult
    assert isinstance(series[1], Serie)
    assert series[1].name == 'Série2'


def test_add_and_save_upcoming_series(file_path):
    upcoming_series = UpcomingSeries(file_path)
    serie = Serie(
        id_serie=3,
        name="Nouvelle Série",
        number_of_seasons=1,
        number_of_episodes=10,
        spoken_languages=["English"],
        first_air_date=datetime.datetime(2024, 5, 1),
        last_air_date=datetime.datetime(2024, 6, 1),
        status="In Production",
        genres=["Drama"],
        networks=["Network1"],
        adult=False
    )
    upcoming_series.add_upcoming_series(serie)
    assert len(upcoming_series.upcoming_series) == 1

    upcoming_series.save_upcoming_series()
    df = pd.read_csv(file_path)
    assert len(df) == 1
    assert df['name'].iloc[0] == 'Nouvelle Série'


def test_show_upcoming_series(file_path, capsys):
    serie1 = Serie(
        id_serie=1,
        name="Série1",
        number_of_seasons=5,
        number_of_episodes=50,
        spoken_languages=["French", "English"],
        first_air_date=datetime.datetime(2024, 1, 1),
        last_air_date=datetime.datetime(2024, 12, 31),
        status="In Production",
        genres=["Drama", "Comedy"],
        networks=["Network1", "Network2"],
        adult=False
    )
    serie2 = Serie(
        id_serie=2,
        name="Série2",
        number_of_seasons=3,
        number_of_episodes=30,
        spoken_languages=["Spanish"],
        first_air_date=datetime.datetime(2024, 2, 1),
        last_air_date=datetime.datetime(2024, 4, 30),
        status="Ended",
        genres=["Action"],
        networks=["Network3"],
        adult=True
    )

    upcoming_series = UpcomingSeries(file_path)
    upcoming_series.upcoming_series = [serie1, serie2]

    upcoming_series.show_upcoming_series()
    captured = capsys.readouterr()
    assert "Séries à venir :" in captured.out
    assert "- Série1" in captured.out
    assert "- Série2" in captured.out

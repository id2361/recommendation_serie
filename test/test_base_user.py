import pytest
from base_user import BaseUser


csv_file = "C:/Users/Céleste/Desktop/1A INGE/projet_info/recommendation_serie/test/test_data_users.csv"


@pytest.fixture
def base_user():
    return BaseUser(csv_file)


def test_add_user(base_user):
    base_user.add_user("Paul", "334", False, "Anglais", "Comédie")
    user = base_user.load_user("Paul")
    assert user.user_name == "Paul"
    assert user.password == "334"


def test_modify_user(base_user):
    base_user.modify_user("Paul", new_language="Français",
                            new_genre_serie="Drame")
    user = base_user.load_user("Paul")
    assert user.language == "Français"
    assert user.genre_serie == "Drame"


def test_delete_user(base_user):
    base_user.delete_user("B")
    user_exists = base_user.verify_user("B")
    assert user_exists == False


def test_verify_user(base_user):
    user_exists = base_user.verify_user("Paul")
    assert user_exists == True


if __name__ == "__main__":
    pytest.main()

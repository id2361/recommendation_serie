from user import User
import pytest


def test_user_creation():
    user = User(1, "Yuzhi", "mdp", True, "Anglais", "Action")
    assert user.user_id == 1
    assert user.user_name == "Yuzhi"
    assert user.password == "mdp"
    assert user.adult
    assert user.language == "Anglais"
    assert user.genre_serie == "Action"


def test_user_creation_invalid():
    with pytest.raises(TypeError):
        User("id", "Yuzhi", "mdp", True, "Anglais", "Action")
    with pytest.raises(TypeError):
        User(1, 123, "mdp", True, "Anglais", "Action")
    with pytest.raises(TypeError):
        User(1, "Yuzhi", 123, True, "Anglais", "Action")
    with pytest.raises(TypeError):
        User(1, "Yuzhi", "mdp", "Yes", "Anglais", "Action")
    with pytest.raises(TypeError):
        User(1, "Yuzhi", "mdp", True, 123, "Action")
    with pytest.raises(TypeError):
        User(1, "Yuzhi", "mdp", True, "Anglais", 123)


def test_user_str():
    user = User(1, "Yuzhi", "mdp", True, "Anglais", "Action")
    expected_str = "1, Yuzhi, True, Anglais, Action"
    assert str(user) == expected_str


if __name__ == "__main__":
    pytest.main()

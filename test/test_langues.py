import pytest
from language import Language
from base_series import BaseSeries
from serie import Serie
import pandas as pd


@pytest.fixture
def base_series():
    # Créer une instance de BaseSeries
    base_series = BaseSeries()
    # Ajouter quelques séries pour les tests
    s1 = Serie(id_serie=1, name="Série 1", number_of_seasons=3,
               number_of_episodes=30, spoken_languages=["fr", "en"],
               first_air_date=pd.to_datetime("2022-01-01"),
               last_air_date=pd.to_datetime("2023-01-01"),
               status="En cours", genres=["Action", "Drame"],
               networks=["Netflix"], adult=False)
    s2 = Serie(id_serie=2, name="Série 2", number_of_seasons=5,
               number_of_episodes=50, spoken_languages=["fr", "es"],
               first_air_date=pd.to_datetime("2020-01-01"),
               last_air_date=pd.to_datetime("2022-01-01"),
               status="Terminée", genres=["Comédie", "Romance"],
               networks=["HBO"], adult=False)

    s3 = Serie(id_serie=3, name="Série 3", number_of_seasons=2,
               number_of_episodes=20, spoken_languages=["en"],
               first_air_date=pd.to_datetime("2021-01-01"),
               last_air_date=pd.to_datetime("2022-01-01"), status="En cours",
               genres=["Science-fiction"], networks=["Amazon Prime"],
               adult=True)
    base_series.series = {1: s1, 2: s2, 3: s3}
    return base_series


def test_series_of_language(base_series):
    # Créer une instance de Langue pour la langue "fr"
    langue_fr = Language("fr")
    # Récupérer les séries en langue française
    series_fr = langue_fr.series_of_language(base_series)
    # Vérifier le nombre de séries récupérées
    assert len(series_fr) == 2
    # Vérifier que les noms des séries sont corrects
    assert all(serie.name in ["Série 1", "Série 2"] for serie in series_fr)

    # Créer une instance de Langue pour la langue "en"
    langue_en = Language("en")
    # Récupérer les séries en langue anglaise
    series_en = langue_en.series_of_language(base_series)
    # Vérifier le nombre de séries récupérées
    assert len(series_en) == 2
    # Vérifier que les noms des séries sont corrects
    assert all(serie.name in ["Série 1", "Série 3"] for serie in series_en)


def test_verify_serie_of_language(base_series):
    # Créer une instance de Langue pour la langue "fr"
    langue_fr = Language("fr")
    # Récupérer une série en langue française
    serie_fr = base_series.series[1]
    # Vérifier que la série est bien en langue française
    assert langue_fr.verify_serie_of_language(serie_fr)

    # Créer une instance de Langue pour la langue "es"
    langue_es = Language("es")
    # Récupérer une série en langue espagnole
    serie_es = base_series.series[3]
    # Vérifier que la série n'est pas en langue espagnole
    assert not langue_es.verify_serie_of_language(serie_es)

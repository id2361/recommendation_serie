import datetime
import pytest
from base_series import BaseSeries
from featured_series import FeaturedSeries, get_new_serie

# Fixtures


@pytest.fixture
def base_series():
    base_series = BaseSeries()
    base_series.load_from_path("C:/Users/Céleste/Desktop/1A INGE/projet_info/recommendation_serie/test/test_data_serie.csv")
    return base_series

# Tests


def test_featured_series_init(base_series):
    featured_series = FeaturedSeries(base_series)
    assert featured_series.base_series == base_series


def test_released_series_less_year(base_series):
    featured_series = FeaturedSeries(base_series)
    series_recentes = featured_series.featured_series_less_year()
    date_actuelle = datetime.datetime.now()
    for serie in series_recentes:
        assert (date_actuelle - serie.last_air_date).days <= 365


def test_get_new_serie(base_series, capsys):
    get_new_serie(base_series)
    captured = capsys.readouterr()
    assert "Séries récentes (sorties il y a moins d'un an) :" in captured.out
    assert "- Riverdale" in captured.out
    assert "- The Flash" in captured.out
    assert "- Grey's Anatomy" in captured.out
    assert "- The Simpsons" in captured.out

from serie import Serie
from base_series import BaseSeries


class Language:
    """Classe représentant une langue de série.

    Attributes:
    -----------
    language : str
        Le nom de la langue.

    Examples:
    ---------
    >>> base_series = BaseSeries()
    >>> base_series.load_from_path("dat_test.csv")
    Base de données chargée avec succès depuis dat_test.csv.
    >>> french=Language("fr")
    >>> list_french=french.series_of_language(base_series)
    >>> print(list_french[0])
    Miraculous: Tales of Ladybug & Cat Noir
    >>> serie=base_series.series[87739]
    >>> french.verify_serie_of_language(serie)
    True
    """

    def __init__(self, language):
        if not isinstance(language, str):
            raise TypeError("La langue doit être une instance de str.")
        self.language = language

    def series_of_language(self, base_series):
        """Récupération de toutes les séries disponibles
        dans cette langue dans la base de données spécifiée.

        Parameters:
        -----------
        base_series: BaseSeries
            Base de données contenant les séries.

        Return:
        -------
        list[Serie]
            Liste des séries qui sont traduites dans la langue.
        """
        if not isinstance(base_series, BaseSeries):
            raise TypeError("La base de série doit être une instance de "
                            "BaseSeries.")
        series_language = []
        for serie in base_series.series.values():
            if self.language in serie.spoken_languages:
                series_language.append(serie)
        return series_language

    def verify_serie_of_language(self, serie):
        """Vérification de la disponibilité d'une série dans la langue.

        Parameters:
        -----------
        serie: Serie
            Série précisée.

        Return:
        -------
        bool
            True si elle est disponible, False sinon.
        """
        if not isinstance(serie, Serie):
            raise TypeError("La série doit être une instance de Serie.")
        return self.language in serie.spoken_languages


def get_series_by_language(language_name, base_series):
    """Procédure affichant les séries appartenant à une langue.

    Parameters:
    -----------
    language_name: str
        Nom du genre.
    base_series: BaseSeries
        Base de données contenant les séries.
    """
    language = Language(language_name)
    series = language.series_of_language(base_series)
    print(f"Séries disponibles en {language_name}:")
    for serie in series:
        print(serie.name)



if __name__ == "__main__":
    import doctest
    doctest.testmod(verbose=True)

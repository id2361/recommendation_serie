# Recommandation de séries

![project-image](dataset-cover.jpeg)


## Introduction

Bienvenue à toutes et à tous ! Nous sommes Maxime Ferret, Konan Léger Kouassi, Céleste Nenehidini et Yuzhi Wang, étudiants en première année l'école ingénieur à l'ENSAI de Bruz. Sous la supervision de Monsieur Valot, nous avons réalisé ce travail dans le cadre de notre projet informatique.
Si vous lisez ce texte, c'est très certainement parce que vous souhaitez en apprendre plus sur notre application de recommandation de séries. Vous êtes au bon endroit ! Cette application est notre réponse à Deniz Bilgin, Ingénieur junior en IA et contributeur sur [Kaggle](https://www.kaggle.com/datasets/denizbilginn/tv-shows) qui proposait plusieurs approches d'applications informatiques possibles sur la base full-tmdb-tv-shows.

Afin de vous guider au mieux dans notre application, voici les 3 points importants que nous allons aborder :
- [Menu principal et Généralités](#main_menu)
- [Administrateur](#administrator)
- [Utilisateur](#user)

En effet, nous allons d'abord voir les prérequis et généralités pour vous assurer le bon fonctionnement de l'application. Ensuite, nous verrons l'usage pour l'administrateur et pour l'utilisateur.

---

## Menu principal et Généralités

Tout d'abord parlons des packages nécessaires. De manière générale, nous vous conseillons de télécharger les dernières mises à jour des packages que nous avons utilisés (date du 6 mai 2024) :

### Version de python requise :
```bash
python --version
Python 3.9.19
```

### doctest (version 2.4.11)
Pour pip :
```bash
pip install --upgrade doctest
```

Pour conda :
```bash
conda install -c conda-forge doctest
```

### pytest (version 8.0.1)
Pour pip :
```bash
pip install --upgrade pytest
```

Pour conda :
```bash
conda install -c conda-forge pytest
```

### tqdm (version 4.66.4)
Pour pip :
```bash
pip install --upgrade tqdm
```

Pour conda :
```bash
conda install -c conda-forge tqdm
```

### scikit-learn (version 1.4.2)
Pour pip :
```bash
pip install --upgrade scikit-learn
```

Pour conda :
```bash
conda install -c conda-forge scikit-learn
```

### pandas (version 2.2.1)
Pour pip :
```bash
pip install --upgrade pandas
```

Pour conda :
```bash
conda install -c conda-forge pandas
```

Parfait ! Tous les packages sont à jour. Quant aux packages 'datetime', 'json' et 'csv', ils sont déjà inclus dans la bilbiothèque standard de Python.

Maintenant, pour démarrer l'application, pour pouvez ouvrir un terminal et entrer la commande suivante :
```bash
python __main__.py
```

class Admin:
    """Classe représentant un Administateur.

    Attributes:
    -----------
    admins: dict
        Dictionnaire des administrateurs.
    connected: bool
        True si un administrateur est connecté, False sinon.

    Examples:
    ---------
    >>> admin = Admin()
    >>> admin.connected
    False
    """
    def __init__(self):
        self.admins = {"Céleste": "0000", "Yuzhi": "1111",
                       "Leger": "2222", "Maxime": "3333", "Mr Valot": "4444"}
        self.connected = False

    def login(self, nom, mdp):
        """Connection d'un administrateur en vérifiant ses identifiants.

        Parameters:
        -----------
        nom : str
            Nom de l'administrateur.
        mdp : str
            Mot de passe de l'administrateur.

        Return:
        -------
        Affiche "Connexion réussie." si l'administrateur a entré ses bons
        identifiants, "Connexion échouée" le cas échéant.

        Examples:
        ---------
        >>> admin = Admin()
        >>> admin.login("Mr Valot", "5555")
        Connexion échouée.
        >>> admin.login("Mr Valot", "4444")
        Connexion réussie.
        >>> admin.connected
        True
        """
        if nom in self.admins and self.admins[nom] == mdp:
            print("Connexion réussie.")
            self.connected = True
        else:
            print("Connexion échouée.")

    def login_page(self):
        """Afficher la page de connexion des administrateurs.

        Return:
        -------
        Connexion pour administrateur
        Nom d'administrateur: (valeur entrée par l'administrateur)
        Mot de passe: (valeur entrée par l'administrateur)
        """
        print("Connexion pour un administateur")
        nom = input("Nom d'administateur : ")
        mdp = input("Mot de passe : ")
        self.login(nom, mdp)


if __name__ == "__main__":
    import doctest
    doctest.testmod(verbose=True)

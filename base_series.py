import pandas as pd
from serie import Serie
import csv
from tqdm import tqdm
from parsed_data import parse_data


class BaseSeries:
    """Classe représentant une base de séries.

    Attributes:
    ----------
    series: dict[id (str): serie (Serie)]

    Examples:
    ---------
    >>> base_series = BaseSeries()
    >>> base_series.load_from_path("data_shows.csv")
    Base de données chargée avec succès depuis data_shows.csv.
    """
    def __init__(self):
        self.series = {}

    def save_to_file(self, file_name):
        """Sauvegarde la base de données dans un fichier csv.

        Parameters:
        -----------
        file_name: str
            Nom du fichier de données avec l'extension '.csv'.
        """
        if not isinstance(file_name, str):
            raise TypeError("Le nom du fichier de données doit être une "
                            "une instance de str.")
        if file_name[-4:] != ".csv":
            raise ValueError("Le fichier doit contenir l'extension .csv")
        with open(file_name, 'w', newline='', encoding='utf-8') as csv_file:
            writer = csv.DictWriter(
                csv_file, fieldnames=self.series.values()[0].to_dict().keys()
            )
            writer.writeheader()
            for serie in self.series.values():
                writer.writerow(serie.to_dict())

    def load_from_path(self, file_name):
        """Chargement des séries à partir d'un chemin d'accès à un fichier de
        données.

        Parameters:
        -----------
        file_name: str
            Nom du fichier de données avec l'extension '.csv'.

        Return:
        -------
        None
        """
        if not isinstance(file_name, str):
            raise TypeError("Le nom du fichier de données doit être une "
                            "une instance de str.")
        if file_name[-4:] != ".csv":
            raise ValueError("Le fichier doit contenir l'extension .csv")

        data = pd.read_csv(file_name, encoding='utf-8')
        for serie in tqdm(range(len(data)), desc="Chargement des séries",
                          unit="lignes"):

            data_from_database = data.iloc[serie]

            new_serie = Serie(*parse_data(
                    data_from_database['id'],
                    data_from_database['name'],
                    data_from_database['number_of_seasons'],
                    data_from_database['number_of_episodes'],
                    data_from_database['spoken_languages'],
                    data_from_database['first_air_date'],
                    data_from_database['last_air_date'],
                    data_from_database['status'],
                    data_from_database['genres'],
                    data_from_database['networks'],
                    data_from_database['adult']
                )
            )
            self.add_serie(new_serie)
        print(f"Base de données chargée avec succès depuis {file_name}.")

    def add_serie(self, new_serie):
        """Ajout d'une nouvelle série à la base de données.

        Parameters:
        -----------
        new_serie: Serie
            Serie à ajouter.

        Return:
        -------
        None
        """
        if not isinstance(new_serie, Serie):
            raise TypeError("La série à ajouter doit être une instance de "
                            "Serie.")
        if new_serie.id_serie not in self.series:
            self.series[new_serie.id_serie] = new_serie

    def delete_serie(self, serie_id):
        """Suppression d'une série de la base de données par son identifiant.

        Parameters:
        -----------
        serie_id: int
            Identifiant de la série à supprimer.

        Return:
        -------
        None
        """
        if serie_id in self.series:
            if serie_id == 456:
                print("Desolé, cette serie ne peut etre supprimer.")
            else:
                del self.series[serie_id]
                print(f"Série avec l'identifiant {serie_id} supprimée de la base"
                      f" de données.")
        else:
            print(f"Série avec l'identifiant {serie_id} non trouvée dans "
                  f"la base de données.")

    def search_serie(self, title):
        """Recherche d'une série par son titre et renvoyer l'objet Serie
        correspondant.

        Parameters:
        -----------
        title: str
            Titre de la série recherchée.

        Return
        ------
        Serie
            Retourne la série recherchée lorsqu'elle est trouvée.
        """
        title_lower = title.lower()
        for serie in self.series.values():
            if serie.name.lower() == title_lower:
                return serie
        print(f"Aucune série trouvée avec le titre '{title}' dans "
              f"la base de données.")

    def get_serie(self, id_serie):
        """Obtention de la série par son identifiant.

        Parameters:
        -----------
        id_serie: int
            Identifiant de la série.

        Return:
        -------
        Serie
            Retourne la série recherchée lorsqu'elle est trouvée.
        """
        if not isinstance(id_serie, int):
            raise TypeError("L'identifiant de la série doit être une instance "
                            "d'int.")
        for serie in self.series.values():
            if serie.id_serie == id_serie:
                return serie
        print(f"Aucune série trouvée avec l'identifiant '{id_serie}' dans "
              f"la base de données.")



if __name__ == "__main__":
    import doctest
    doctest.testmod(verbose=True)

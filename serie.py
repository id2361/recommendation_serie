import datetime


class Serie:
    """Classe représentant une série.

    Parameters:
    -----------
    id_serie : int
        ID de la série.
    name : str
        Nom de la série.
    number_of_seasons : int
        Nombre de saisons.
    number_of_episodes : int
        Nombre d'épisodes.
    spoken_languages : list[str]
        Langues parlées.
    first_air_date : datetime.datetime
        Date de première diffusion.
    last_air_date : datetime.datetime
        Date de dernière diffusion.
    status : str
        Statut de la série.
    genres : list[str]
        Genres de la série.
    networks : list[str]
        Réseaux de distribution de la série.
    adult : bool
        Indique si la série est destinée aux adultes.

    Examples:
    ---------
    >>> serie = Serie(id_serie=1, name="Breaking Bad", number_of_seasons=5,
    ...               number_of_episodes=62, spoken_languages=["English"],
    ...               first_air_date=datetime.datetime(2008, 1, 20),
    ...               last_air_date=datetime.datetime(2013, 9, 29),
    ...               status="Ended", genres=["Drama", "Crime"],
    ...               networks=["AMC"], adult=False)
    >>> serie.id_serie
    1
    >>> serie.name
    'Breaking Bad'
    >>> serie.number_of_seasons
    5
    >>> serie.number_of_episodes
    62
    >>> serie.spoken_languages
    ['English']
    >>> serie.first_air_date
    datetime.datetime(2008, 1, 20, 0, 0)
    >>> serie.last_air_date
    datetime.datetime(2013, 9, 29, 0, 0)
    >>> serie.status
    'Ended'
    >>> serie.genres
    ['Drama', 'Crime']
    >>> serie.networks
    ['AMC']
    >>> serie.adult
    False

    >>> serie_invalide = Serie(id_serie=2, name="Friends",
    ...                         number_of_seasons=10,
    ...                         number_of_episodes=236,
    ...                         spoken_languages=["English"],
    ...                         first_air_date="1994-09-22",
    ...                         last_air_date=datetime.datetime(2004, 5, 6),
    ...                         status="Ended", genres=["Comedy"],
    ...                         networks=["NBC"], adult=False)
    Traceback (most recent call last):
    ...
    TypeError: La date de première diffusion doit être une instance de \
datetime.
    >>> serie.to_dict()
    {'id': 1, 'name': 'Breaking Bad', 'number_of_seasons': 5, \
'number_of_episodes': 62, 'spoken_languages': 'English', \
'first_air_date': '2008-01-20 00:00:00', 'last_air_date': \
'2013-09-29 00:00:00', 'status': 'Ended', 'genres': 'Drama, Crime', \
'networks': 'AMC', 'adult': False}
    """
    def __init__(self, id_serie, name,
                 number_of_seasons, number_of_episodes,
                 spoken_languages,
                 first_air_date, last_air_date, status,
                 genres, networks, adult):
        if not isinstance(id_serie, int):
            raise TypeError("L'ID de la série doit être une instance de int.")
        if not isinstance(name, str):
            raise TypeError(
                "Le titre de la série doit être une instance de str.")
        if not isinstance(number_of_seasons, int):
            raise TypeError("Le nombre de saisons doit être une instance de "
                            "int.")
        if not isinstance(number_of_episodes, int):
            raise TypeError("Le nombre d'épisodes doit être une instance de "
                            "int.")
        if not isinstance(genres, list):
            raise TypeError("Les genres doivent être une instance de list.")
        for element in genres:
            if not isinstance(element, str):
                raise TypeError("Les valeurs des genres doivent être des "
                                "instances de str.")
        if not isinstance(spoken_languages, list):
            raise TypeError("Les langues doivent être une instance de list.")
        for element in spoken_languages:
            if not isinstance(element, str):
                raise TypeError("Les valeurs des langues doivent être des "
                                "instances de str.")
        if not isinstance(adult, bool):
            raise TypeError("La valeur indiquant si la série est destinée aux "
                            "adultes doit être une instance de bool.")
        if not isinstance(status, str):
            raise TypeError("Le statut de la série doit être une instance "
                            "de str.")
        if not isinstance(first_air_date, datetime.datetime):
            raise TypeError("La date de première diffusion doit être une "
                            "instance de datetime.")
        if not isinstance(last_air_date, datetime.datetime):
            raise TypeError("La date de dernière diffusion doit être une "
                            "instance de datetime.")
        if not isinstance(networks, list):
            raise TypeError("Les réseaux de distribution doivent être une "
                            "instance de list.")
        self.id_serie = id_serie
        self.name = name
        self.number_of_seasons = number_of_seasons
        self.number_of_episodes = number_of_episodes
        self.spoken_languages = spoken_languages
        self.first_air_date = first_air_date
        self.last_air_date = last_air_date
        self.status = status
        self.genres = genres
        self.networks = networks
        self.adult = adult

    def to_dict(self):
        """Convertion d'un objet Serie en un dictionnaire.

        Return:
        -------
        dict
            Dictionnaire représentant la série.
        """
        return {
            'id': self.id_serie,
            'name': self.name,
            'number_of_seasons': self.number_of_seasons,
            'number_of_episodes': self.number_of_episodes,
            'spoken_languages': ', '.join(self.spoken_languages),
            'first_air_date': str(self.first_air_date),
            'last_air_date': str(self.last_air_date),
            'status': self.status,
            'genres': ', '.join(self.genres),
            'networks': ', '.join(self.networks),
            'adult': self.adult
        }

    def __str__(self):
        return f"{self.name}"


if __name__ == "__main__":
    import doctest
    doctest.testmod(verbose=True)

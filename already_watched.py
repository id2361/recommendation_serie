from user import User
from serie import Serie


class AlreadyWatched:
    """Classe représentant les séries déjà suivies par un utilisateur.

    Attributes:
    -----------
    user: User
        L'utilisateur associé à la liste des séries déjà suivies.
    series_watched: List[Serie]
        Liste des séries déjà suivies par l'utilisateur.


    Examples
    --------
    >>> import datetime
    >>> utilisateur = User(1, "John", "password", False, "fr", "Drame")
    >>> deja_suivie = AlreadyWatched(utilisateur)
    >>> serie1 = Serie(1, "Breaking Bad", 5, 62, ["en"],
    ...                datetime.datetime(2008,1,20),
    ...                datetime.datetime(2013,9,29),
    ...                "Ended", ["Drama", "Crime", "Thriller"],
    ...                ["AMC"], False)
    >>> serie2 = Serie(2, "Stranger Things", 3, 25, ["en"],
    ...                datetime.datetime(2016,7,15),
    ...                datetime.datetime(2019,7,4), "Ended",
    ...                ["Drama", "Fantasy", "Horror"],
    ...                ["Netflix"], False)
    >>> deja_suivie.add_serie_watched(serie1)
    >>> deja_suivie.add_serie_watched(serie2)
    >>> serie1 in deja_suivie.series_watched
    True
    >>> deja_suivie.delete_serie_watched(serie1)
    >>> serie1 in deja_suivie.series_watched
    False
    """
    def __init__(self, user):
        if not isinstance(user, User):
            raise TypeError("L'utilisateur doit être une instance de User.")
        self.user = user
        self.series_watched = []

    def add_serie_watched(self, serie):
        """Ajout d'une série à la liste des séries déjà suivies par
        l'utilisateur.

        Parameters:
        -----------
        serie: Serie
            Série déjà suivie par l'utilisateur.

        Return:
        -------
        None
        """
        if not isinstance(serie, Serie):
            raise TypeError("La serie doit être une instance de Serie.")
        self.series_watched.append(serie)

    def delete_serie_watched(self, serie):
        """Suppression d'une série de la liste des séries déjà suivies par
        l'utilisateur.

        Parameters:
        -----------
        serie: Serie
            Série déjà suivie par l'utilisateur.

        Return:
        -------
        None
        """
        if not isinstance(serie, Serie):
            raise TypeError("La serie doit être une instance de Serie.")
        if serie in self.series_watched:
            self.series_watched.remove(serie)
        else:
            print("Cette série n'est pas dans la liste"
                  "des séries suivies par cet utilisateur.")


if __name__ == "__main__":
    import doctest
    doctest.testmod(verbose=True)
